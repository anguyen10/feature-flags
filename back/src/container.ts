import { Container } from "inversify";

import { IToggle } from "./models/toggle/IToggle";
import { ToggleOff } from "./models/toggle/ToggleOff";
import { ToggleOn } from "./models/toggle/ToggleOn";

import { selector } from "./utils/flag";

let container = new Container();

container.bind(IToggle).toDynamicValue(() => selector("demo-flag", ToggleOn, ToggleOff));

export { container };

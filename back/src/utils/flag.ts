import { unleash } from "../../index";

export const selector = (flag: string, onEnabled, onDisabled) => {
    const Component = unleash?.isEnabled(flag) ? onEnabled : onDisabled;
    return new Component()
}

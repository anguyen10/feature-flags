import * as express from 'express';
import { controller, httpGet, request, response } from "inversify-express-utils";
import { inject } from "inversify";
import { IToggle } from "../models/toggle/IToggle";

@controller('/')
export class Controller {
    constructor(@inject(IToggle) private toggle: IToggle) {}

    @httpGet('/')
    public checkToggle(@request() req: express.Request, @response() res: express.Response) {
        return res.send({
            status: 'success',
            data: this.toggle.check()
        })
    }
}
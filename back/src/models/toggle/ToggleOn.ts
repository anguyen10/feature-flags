import { IToggle } from "./IToggle";

export class ToggleOn extends IToggle {
  constructor() {
    super();
  }

  check() {
    return "The flag 'demo-flag' is enabled."
  }
}

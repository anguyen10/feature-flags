import { injectable } from "inversify";

@injectable()
export abstract class IToggle {
    abstract check(): string
}

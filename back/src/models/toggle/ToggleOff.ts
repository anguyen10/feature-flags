import { IToggle } from "./IToggle";

export class ToggleOff extends IToggle {
  constructor() {
    super();
  }

  check() {
    return "The flag 'demo-flag' is disabled."
  }
}

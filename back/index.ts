import "reflect-metadata";
import './src/routes/Controller';
import { container } from "./src/container";
import { initialize } from "unleash-client";
import { InversifyExpressServer } from "inversify-express-utils";
import * as http from "http";

export const unleash = initialize({
  url: "https://gitlab.com/api/v4/feature_flags/unleash/37539146",
  appName: "feature-flags",
  instanceId: "-NLLhL8LidwNnny6WzUX"
});

const checkFlagStatus = (flagName) => {
  const isFlagEnabled = unleash.isEnabled(flagName)
  console.log(`The flag '${flagName}' is ${isFlagEnabled ? 'enabled' : 'disabled'}`);
}

unleash.on("synchronized", () => {
  console.log('Synchronizing to get flags status.')
  checkFlagStatus('demo-flag')
})

unleash.on("changed", () => {
  console.log('A flag status has changed.')
  checkFlagStatus('demo-flag')
})

const server = new InversifyExpressServer(container)
const app = server.build()
const httpServer = http.createServer(app)
const port = 3002

httpServer.listen(port, () => {
  console.log(`Listening on port ${port}.`)
})
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import { FlagProvider } from '@unleash/proxy-client-react';

const config = {
  url: 'http://localhost:3000/proxy',
  clientKey: 'some-secret',
  refreshInterval: 15,
  appName: 'feature-flags',
};

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <FlagProvider config={config}>
      <App />
    </FlagProvider>
  </React.StrictMode>
);
import React, { useEffect } from 'react';
import { useUnleashContext, useFlag } from '@unleash/proxy-client-react';

function App() {
  const userId = '123'
  const enabled = useFlag('user-flag');
  const updateContext = useUnleashContext();

  useEffect(() => {
    updateContext({ userId })
  }, [userId])

  console.log(userId, enabled)

  return (
    <div className="App">
      <div>Current user: {userId}</div>
      <div>The flag is {enabled ? 'enabled' : 'disabled'}</div>
      {enabled && <button>Screen Recording</button>}
    </div>
  );
}

export default App;
